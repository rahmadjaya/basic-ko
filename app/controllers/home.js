var express = require('express'),
  router = express.Router(),
  Article = require('../models/article');

module.exports = function (app) {
  app.use('/', router);
};

router.get('/', function (req, res, next) {
  var articles = [new Article(), new Article()];
    res.render('index', {
      title: 'Generator-Express MVC',
      articles: articles
    });
});
router.get('/reservations', function (req, res, next) {
  var articles = [new Article(), new Article()];
    res.render('reservations', {
      title: 'Generator-Express MVC',
      articles: articles
    });
});
router.get('/creatui', function (req, res, next) {
  var articles = [new Article(), new Article()];
    res.render('creatui', {
      title: 'Generator-Express MVC',
      articles: articles
    });
});
router.get('/introduction', function (req, res, next) {
  var articles = [new Article(), new Article()];
    res.render('introduction', {
      title: 'Generator-Express MVC',
      articles: articles
    });
});
router.get('/loading', function (req, res, next) {
  var articles = [new Article(), new Article()];
    res.render('loading', {
      title: 'Generator-Express MVC',
      articles: articles
    });
});
router.get('/custom', function (req, res, next) {
  var articles = [new Article(), new Article()];
    res.render('custom', {
      title: 'Generator-Express MVC',
      articles: articles
    });
});
