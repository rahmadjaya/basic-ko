function SeatReservation(name, initialMeal) {
    var self = this;
    self.name = name;
    self.meal = ko.observable(initialMeal);
    
    self.formattedPrice = ko.computed(function(){
        var price = self.meal().price;
        return price ? "$" + price.toFixed(2) : "None";
    });
    
    
}
function Task(data){
    this.title = ko.observable(data.title);
    this.isDone = ko.observable(data.isDone);
}
// Overall viewmodel for this screen, along with initial state
function ReservationsViewModel(pointsBugdet) {
    var self = this;
    
    //RESERVATIONS
    // Non-editable catalog data - would come from the server
    self.availableMeals = [
        { mealName: "Standard (sandwich)", price: 0 },
        { mealName: "Premium (lobster)", price: 34.95 },
        { mealName: "Ultimate (whole zebra)", price: 290 }
    ];    

    // Editable data
    self.seats = ko.observableArray([
        new SeatReservation("Steve", self.availableMeals[0]),
        new SeatReservation("Bert", self.availableMeals[0]),
        new SeatReservation("jhon", self.availableMeals[0])
    ]);
    
    self.addSeat = function(){
        var nn =  self.name = ko.observable(" ");
        self.seats.push(new SeatReservation(nn, self.availableMeals[0]));
    }
    
    self.removeSeat = function(seat){
        self.seats.remove(seat);
    }
    
    self.totalSurcharge = ko.computed(function(){
        var total = 0;
        for (var i=0; i< self.seats().length; i++)
            total+= self.seats()[i].meal().price;
        return total;
    });

    //CREAT UI
    self.folders = ['Inbox', 'Archive', 'Sent', 'Spam'];
    self.chosenFolderId = ko.observable();
    self.chosenFolderData = ko.observable();
    // Behaviours    
    self.goToFolder = function(folder) { 
        self.chosenFolderId(folder); 

        
        $.get(data, { folder: folder }, self.chosenFolderData);
        // // $.getJSON('/json/data.json', function(json){
            
        // // })
        
    }; 
    // self.goToFolder('Inbox');
    
    //INTRO
    this.firstName = ko.observable("Nama Depan");
    this.lastName = ko.observable("Nama Belakang");
    this.capitalizeLastName = function(){
        var currentVal = this.lastName();
        this.lastName(currentVal.toUpperCase());
    }

    //LOADING
    self.tasks = ko.observableArray([]);
    self.newTaskText = ko.observable();
    self.incompleteTasks = ko.computed(function(){
        return ko.utils.arrayFilter(self.tasks(), function(task){
            return !task.isDone()
        });
    });
    self. addTask = function(){
        self.tasks.push(new Task({title: this.newTaskText()}));
        self.newTaskText("");
    };
    self.removeTask = function(task){
        self.tasks.remove(task);
    };

    //CUSTOM
    self.pointsBugdet = 10;
    self.selek = ko.observableArray([
        {textSelect:'pertama'}, 
        {textSelect:'kedua'},
        {textSelect:'ketiga'}, 
        {textSelect:'keempat'}
    ]);

    self.points = ko.observable();
    self.pointsUsed = ko.computed(function(){
        var total = 0;
        for (var i = 0; i < 10; i++) {
            total+= this.points();
            return total;
        };
    }, self);


}

ko.applyBindings(new ReservationsViewModel());





